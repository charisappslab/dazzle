Project Dazzle
===

Codename: 	Dazzle  
Powered by:	CHARIS AppsLab  
Lead by: 	Kevin Tanjung  


Developed on PHP 5.5.12 on Laravel 4.1.2 Framework  
Front-End: Ember.js and Twitter Bootstrap  
Server: Nginx 1.4.0  
Other Dependencies: ReactPHP & Ratchet  